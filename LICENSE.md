# Terminal++ License

`Terminal++` is distributed under the [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) license. 

> This is for now because I had to fill something in. In the future when `tpp` is more mature the license will change into something much more permissive for non-commercial use. 


