# Contributing to *Terminal++*

Thank you for your interest in `tpp`!

## Bugs

For reporting bugs, please use the issue tracker at Bitbucket. Make sure that the issue is minimal and reproducible, i.e. provide set of steps that recreate the bug. Make sure to specify the platform used in detail. 

For bugfixes, please use pull requests. 

## New Features

Please use pull-requests. Unless the feature already has an issue confirmed by the authors, it is best to check first whether a feature is wanted. 